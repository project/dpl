/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/
var _this = this;

var determineRandomSize = function determineRandomSize(min, max) {
  var num = Math.random() * (max - min) + min;

  return parseInt(num, 10);
};

var convertFullWidth = function convertFullWidth(width, fullWidth) {
  return width === -1 ? fullWidth : width;
};

var getInputSizeRanges = function getInputSizeRanges(fullWidth, $, sizeLinks) {
  var inputSizes = sizeLinks.map(function () {
    return $(this).attr("data-dpl-size");
  });
  return Array.from(inputSizes).reduce(function (acc, dplSize) {
    var sizes = dplSize.split(':');
    acc[dplSize] = [convertFullWidth(parseInt(sizes[0], 10), fullWidth), convertFullWidth(parseInt(sizes[1], 10), fullWidth)];
    return acc;
  }, {});
};

var resizeIframe = function resizeIframe($sgWrapper, $sgViewport, size) {
  var width = size === -1 ? $sgWrapper.width() : size;

  $sgViewport.width(width);
  $sgViewport.height("800px");
};

(function (w, $) {
  var $sgWrapper = $("#sg-gen-container");
  var fullWidth = $sgWrapper.width();

  var $sgViewport = $("#sg-viewport");

  var $sizeLinks = $("[data-dpl-size]");
  var inputSizeRanges = getInputSizeRanges(fullWidth, $, $sizeLinks);

  $sizeLinks.on("click", function (e) {
    e.preventDefault();
    var widthIndex = $(e.target).attr("data-dpl-size");
    var widthRange = inputSizeRanges[widthIndex];
    var width = determineRandomSize(widthRange[0], widthRange[1]);

    $(e).parent().toggleClass("active");
    resizeIframe($sgWrapper, $sgViewport, width);
  });

  var initialWithIndex = $("[data-dpl-default]").attr("data-dpl-size");
  var widthRange = inputSizeRanges[initialWithIndex];
  var width = determineRandomSize(widthRange[0], widthRange[1]);
  resizeIframe($sgWrapper, $sgViewport, width);

  var origViewportWidth = $sgViewport.width();
  $sgWrapper.width(origViewportWidth);
  $sgViewport.width(origViewportWidth - 14);
})(this, jQuery);
